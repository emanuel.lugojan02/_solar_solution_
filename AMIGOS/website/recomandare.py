from .our_classes.Regulator_with_invertor_class import *
import sys
from .our_classes.panou_class import *
from .our_classes.accumulator_class import *
import json


def load_region_dict(address = "AMIGOS/website/database/regiuni.json"):
    try:
        file = open(address, "r") #opening the file with the address
        data = json.load(file) #and get the data from it
        return data #return the data if the file exists
    except FileExistsError: #raise an error if the file doesnt exist or it isnt accessible
        raise "FileReadError"
    except FileNotFoundError:
        raise "FileNotFound"


def apply_percent(x,y):
    try:
        y=(y*x)//100
    except:
        y=1
    return y


# determin eficienta regiunii in care se afla user-ul
def get_region_effic(region_dict,city):
    for i in range(1,6):
        if city in region_dict[str(i)]:
            return(70+6*i)


# returnez numarul de acumulatori potrivti pentru panourile recomandate
def get_accumulator_system(all_accumulators:list,required_capacity:int):
    
    d={}
    for i in range(len(all_accumulators)):
        
        if sistem_curent == 2 and all_accumulators[i].capacity > restrictie_baterie - 30:
            continue
        
        sum=0 # capacitatea totala a unui tip de acumulator
        k=0 #numarul de acumulatori
        while sum<required_capacity:
            sum+=all_accumulators[i].capacity
            k+=1 
        pret = k*all_accumulators[i].price      
        d[i]=[all_accumulators[i].name, sum,k,pret]
    pret_min=sys.maxsize
    #cautam cea mai eficienta optiune
    for keys,value in d.items():
        if value[3]<pret_min: 
            pret_min=value[3]
            ind=keys
    return (all_accumulators[ind], d[ind][2],d[ind][3])


restrictie_putere = -1
restrictie_baterie = -1
sistem_curent = 0
# calculez numarul de panouri ce incap in suprafata userului, ce ofera cea mai buna putere pt conditiile userului 
def update_sistem(val):
    global sistem_curent
    sistem_curent = val
def change_restrictie_putere(val):
    global restrictie_putere
    restrictie_putere = val

def change_restrictie_baterie(val):
    global restrictie_baterie
    restrictie_baterie = val

def get_panel_system(all_panels,user_buget,user_length,user_width):



    max_total_power=0 # variabila reprezinta puterea totala maxima care a fost atinsa
    max_ratio = 0
    total_price=0# variabila reprezinta pretul final
    number_of_panels=0#variabila reprezinta numarul de panouri
    index_panel=-1# variabila reprezinta indexul panoului gasit
    for i in range(len(all_panels)): #parcurgem lista panourilor

        if sistem_curent == 1 and all_panels[i].power > restrictie_putere - 30:
            continue
        if sistem_curent == 2 and all_panels[i].power < restrictie_putere + 30:
            continue

        


        if ((all_panels[i].length<=user_length and all_panels[i].width<=user_width) or (all_panels[i].width<=user_length and all_panels[i].length<=user_width)):
            # daca suprafata panoului este mai mica decat suprafata acoperisului
            if user_length//all_panels[i].length*user_width//all_panels[i].width>user_length//all_panels[i].width*user_width//all_panels[i].length:
                # pozitie verticala
                max_number_of_panel=user_length//all_panels[i].length*user_width//all_panels[i].width
            else:
                # pozitie orizontala
                
                max_number_of_panel=user_length//all_panels[i].width*user_width//all_panels[i].length

            contor=0 # variabila este folosita pentru a numara cate panouri putem avea
            current_price=0
            current_power=0

            # check current power not to overpass 14000W, because i dont have bigger invertors then 15kW
            while contor<max_number_of_panel and current_price+all_panels[i].price<=user_buget:
                # cat timp nu depasim numarul maxim posibil de panouri si bugetul utilizatorului, crestem numarul de panouri, pretul si puterea
                contor+=1
                
                
                current_price+=all_panels[i].price
                current_power+=all_panels[i].power

            
            if (current_power>max_total_power) or (current_power==max_total_power and current_price<total_price):
                # cautam metoda care genereaza cel mai mare total de putere;
            # iar in cazul in care avem mai multe metode cu aceeasi putere, alegem metoda in care pretul final este mai mic
                max_total_power=current_power
                total_price=current_price
                number_of_panels=contor
                index_panel=i
            # try:
            #     ratio = current_power/current_price
            # except:
            #     ratio = 0
            # if (restrictie_unu != - 1) and (current_power>max_total_power) and (contor>number_of_panels) and (ratio > max_ratio):
            #     max_total_power=current_power
            #     total_price=current_price
            #     number_of_panels=contor
            #     index_panel=i
            #     max_ratio = ratio

    # if i overpass 14kW i need to ditch some of the panels because my biggest invertor is 15kW
    while max_total_power > 14000:
        max_total_power -= all_panels[index_panel].power
        total_price -= all_panels[index_panel].price
        number_of_panels -= 1

    if index_panel!=-1: # daca am gasit metoda care genereaza cel mai mare total de putere, o returnam
        return (all_panels[index_panel],number_of_panels,max_total_power,total_price)
    return None


#Rolul funtiei este de a cauta cel mai ieftin regulator care sa corespunda cerintelor minime.
#Parametrii functiei sunt lista all_regulators_with_invertors, care contine obiecte de tip Regulator_with_invertor.
#Se creeaza variabila reg_exist care verifica daca exista un regulator pentru cerintele date.
#Apoi, functia verifica fiecare regulator din lista all_regulators_with_invertors daca corespunde puterii cerute.
#In cazul in care un regulator indeplineste cerinta, este salvat in variabila regulator_invertor, iar daca exista deja unul se va compara pretul si daca noul regulator
#este mai ieftin decat cel vechi, acesta v-a fi inlocuit.
#Functia returneaza "None" in cazul in care nu s-a gasit un regulator sau variabila regulator_invertor in caz contrar.
# caut un regulator destul de puternic pentru puterea panourilor 
def get_regulator_invertor_system(all_regulators_with_invertors: list,required_power: int):
    reg_exist = 0 #check if regulator exists
    for i in range(len(all_regulators_with_invertors)):
        if all_regulators_with_invertors[i].power >= required_power:
            if reg_exist == 0:
                regulator_invertor = all_regulators_with_invertors[i]#atribui variabilei regulator_invertor primul regulator care indeplineste conditiile
                reg_exist = 1#indice element    
                point = i #indice element
            else:
                if all_regulators_with_invertors[i].price <= all_regulators_with_invertors[point].price:
                    regulator_invertor = all_regulators_with_invertors[i]#daca gasesc un regulator care respecta cerintele, 
                                                                        # dar are un pret mai mic, actualizez variabla regulator_invertor
                    point = i

    if reg_exist==0:
        return None#nu am gasit un regulator potrivit
    else:
        return regulator_invertor#returnez regulator pe care l-am gasit



# returnez sistemul intreg
def get_full_system(user_budget: int,
                    user_width: float,
                    user_length: float,
                    user_location: str,
                    panel_list: list,
                    accumulator_list: list,
                    regulators_with_invertors_list: list,
                    region_dict: dict):
    
    remaining_budget = user_budget#we will work with this variable so that we don't modify the user budget
    i = 0.2 #procentul 20%
    ok = 0
    positive_result = ((None, None, None, None), (None, None, None), None, None)
    while remaining_budget >= 0.02 * user_budget and i <= 1: #verificam ca bugetul ramas > 2% din bugetul total, si ca i nu depaseste 100%
        ok = 1 #verificam ca exista macar un panou
        panels = get_panel_system(panel_list, int(user_budget * i), user_length, user_width)
        old_power = panels[2]
        power = apply_percent(panels[2], get_region_effic(region_dict, user_location))
        accumulators = get_accumulator_system(accumulator_list, power * 5 / 12) # daca am 300W putere, trebuie sa cacluez W per o zi, adica 300 * 5 = 1500, 
                                                                                # de acolo ca sa aflu amperii, impart la numarul de 12V, 1500/12V => A
        regulators = get_regulator_invertor_system(regulators_with_invertors_list, old_power + 1000) #I need to add extra 1000W for safe use
        i += 0.02 #crestem procentul cu 2%

        # print(i)
        remaining_budget = user_budget - panels[3] - accumulators[2] - regulators.price

        if remaining_budget > 0:
            positive_result = (panels, accumulators, regulators, remaining_budget)
   
    return positive_result

# print(get_full_system(3000, 10, 10, "Timis", load_all_panels(), load_all_accumulators(), load_all_regulators(), load_region_dict()))