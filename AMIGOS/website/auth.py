from os import error
import re
from sys import flags
from types import new_class
from flask import Blueprint, render_template, request, redirect, url_for
import flask
from flask.helpers import flash
from flask_login import login_user, login_required, logout_user, current_user
import json

from .models import *
from werkzeug.security import generate_password_hash, check_password_hash

auth = Blueprint('auth', __name__)



new_user = {
    "phone" : "",
}
old_info = {}
def reset_new_user():
    global new_user
    new_user = {
    'name' : "",
    'email' : "",
    'password' : "",
    'phone' : "",
    'county' : "",
    'city' : "",
    'roof_length' : 0.0,
    'roof_width' : 0.0,
    'month' : "",  
    'consumption' : 0,
}

def reset_old_info():
    global old_info
    old_info = {
    'name' : "",
    'email_sign_up' : "",
    'email_login' : "",
    'password' : "",
    'phone' : "",
    'county' : "",
    'city' : "",
    'roof_length' : "",
    'roof_width' : "",
    'month' : "",  
    'consumption' : "",
}

@auth.route('/consumption',  methods=['GET', 'POST'])
def consumption():
    data = request.form

    if request.method == 'POST':
        month = data.get('month')
        consumption = data.get('consumption')

        # new_user['month'] = month
        # new_user['consumption'] = consumption
        try:
            name = new_user['name']
            email = new_user['email']
            password = new_user['password']
            phone = new_user['phone']
            county = new_user['county']
            city = new_user['city']
            roof_length = new_user['roof_length']
            roof_width = new_user['roof_width']

            old_info['month'] = month
            old_info['consumption'] = consumption


            user = User(name= name, email= email, password=generate_password_hash(password, method='sha256'), phone=phone, county=county, city=city, roof_length=roof_length, roof_width=roof_width, month=month, consumption=consumption, budget = 0)
            db.session.add(user)
            newStat = Stats.query.first()
            newStat.nr_users += 1
            db.session.commit()

            login_user(user, remember=True)
        except:
            pass

        return redirect(url_for('views.home', user=current_user))
    
    return render_template('consumption.html', user=current_user, old_info=old_info)

@auth.route("/devices_or_bill")
def devices_or_bill():
    return render_template("devices_or_bill.html")

@auth.route('/Surface', methods=['GET', 'POST'])
def Surface():
    data = request.form

    if request.method == 'POST':
        roof_length = data.get('length')
        roof_width = data.get('width')

        new_user['roof_length'] = roof_length
        new_user['roof_width'] = roof_width

        old_info['roof_length'] = roof_length
        old_info['roof_width'] = roof_width

        print(roof_length, roof_width)

        return redirect(url_for('auth.devices_or_bill', user=current_user, old_info=old_info))

    return render_template('Surface.html', user=current_user, old_info=old_info)

@auth.route('/SIgnInSignUp', methods=['GET', 'POST'])
def SIgnInSignUp():
    data = request.form
    reset_new_user()
    reset_old_info()

    if request.method == 'POST':

        state = data.get('state')
        old_info['state'] = state
        if state == 'create':
        
            global new_user
            
            name = data.get('name')
            email = data.get('email')
            password = data.get('password')
            
            
            phone = "0.67;0.23"
            
            
            county = data.get('county')
            city = data.get('city')

            new_user['name'] = name
            new_user['email'] = email
            new_user['password'] = password
            new_user['phone'] = phone
            new_user['county'] = county
            new_user['city'] = city

            old_info['name'] = name
            old_info['email_sign_up'] = email
            old_info['password'] = password
            old_info['phone'] = phone
            old_info['county'] = county
            old_info['city'] = city

            print(new_user)
            print(f'STATE ____________ {state}')


            search = User.query.filter_by(email=email).first()
            errors = []
            if search:
                flash('This email is already being used by another account, please try another one')
                errors.append('email_sign_up')

            if errors == []:
                return redirect(url_for('auth.GetToKnow', user=current_user))
            else:
                return render_template('SIgnInSignUp.html', user=current_user, errors=errors, old_info=old_info)
        else:
            errors = []
            print(f'STATE_____________ {state}')

            email = data.get('email')
            password = data.get('password')

            old_info['email_login'] = email

            my_user = User.query.filter_by(email= email).first()

            if my_user:
                if check_password_hash(my_user.password, password):
                    # flash('Credentials are corect, redirecting to home page', category='success')
                    login_user(my_user, remember=True)
                    return redirect(url_for('views.home', user=current_user))
                else:
                    flash('Your password is incorrect, please try again', category='error')
                    errors.append('password')
            else:
                flash('Your email is incorrect, please try again', category='error')
                errors.append('email_login')

            return render_template('SIgnInSignUp.html', user=current_user, errors=errors, old_info=old_info)


    # while 1 == 1:
    #     db.session.query(User).delete()
    #     db.session.commit()
    #     break
    return render_template('SIgnInSignUp.html', user=current_user, errors=[], old_info=old_info)


@auth.route('/GetToKnow')
def GetToKnow():

    return render_template('GetToKnow.html', user=current_user)




@auth.route('/login', methods=['GET', 'POST'])
def login():
    data = request.form

    # delete entire User table
    # while 1 == 1:
    #     db.session.query(User).delete()
    #     db.session.commit()
    #     break
    global old_info
    old_info = []

    if request.method == 'POST':
        email = data.get('email')
        password = data.get('password')

        old_info.append(email)

        my_user = User.query.filter_by(email= email).first()
        if my_user:
            if check_password_hash(my_user.password, password):
                flash('Credentials are corect, redirecting to home page', category='success')
                login_user(my_user, remember=True)
                return redirect(url_for('views.home', user=current_user))
            else:
                flash('Your password is incorrect, please try again', category='error')
        else:
            flash('Your email is incorrect, please try again', category='error')

    return render_template("login.html", old_info=old_info, user=current_user)

@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('views.home'))


def load_cities():
   orase = {}
   with open('AMIGOS/website/database/orase.json', 'r') as file:
      orase = json.load(file)
      for key in orase.keys():
          orase[key] = sorted(orase[key])

   return orase

def load_months():
    return ['Ianuarie', 'Februarie', 'Martie', 'Aprilie', 'Mai', 'Iunie', 'Iulie', 'August', 'Septembrie', 'Octombrie', 'Noiembrie', 'Decembrie']


@auth.route('/sign-up', methods=['GET', 'POST'])
def sign_up():
    data = request.form
    old_info = ["", "", "", 'Alba', "", "", "", 'Ianuarie', ""]
    error = 0

    orase = load_cities()
    months = load_months()


    if request.method == 'POST':
        name = data.get('name')
        email = data.get('email')
        password = data.get('password')
        county = data.get('county')
        city = data.get('city')
        roof_length = data.get('roof_length')
        roof_width = data.get('roof_width')
        month = data.get('month')
        consumption = data.get('consumption')
        phone = data.get('phone')
        
        
        
        refresh = data.get('refresh')




        print(f'This is the county {county}')
        
        
        print(f"AICI BA {data.get('refresh')}")


        old_info = [name, email, password, county, city, roof_length, roof_width, month, consumption, phone]

        flash = flask.flash

        my_user = User.query.filter_by(email= email).first()

        if refresh != 'refresh':

            if len(name) < 2:
                flash('Name must have atleast 2 characters', category='error')
                error = 1
            if len(email) < 4:
                flash('Email must have at least 4 characters', category='error')
                error = 1
            if my_user:
                flash('This email is being used by another account, try a different one', category='error')
                error = 1
            if len(password) < 6:
                flash('Password must have a length of at least 6 characters', category='error')
                error = 1

            if roof_length == "":
                flash('Please enter a roof length', category='error')
                error = 1
            if roof_width == "":
                flash('Please enter a roof width', category='error')
                error = 1
            if consumption == "":
                flash("Please enter a consumption", category='error')
                error = 1

            if error == 0:
                new_user = User(name= name, email= email, password=generate_password_hash(password, method='sha256'), phone=phone, county=county, city=city, roof_length=roof_length, roof_width=roof_width, month=month, consumption=consumption)

                print(f'The name of the new user is {new_user.name}')
                login_user(new_user, remember=True)
                db.session.add(new_user)
                db.session.commit()
                flash('Account created', category='succes')

                return redirect(url_for('views.home'))


    return render_template("sign-up.html", months=months, orase=orase, old_info=old_info, user=current_user)



def append_device_to_phone(device: list):
    for i in range(len(device)):
        split_phone = new_user['phone'].split(";")
        split_phone.append(device[i])
        new_user['phone'] =  ";".join(split_phone)
    

def sign_in_new_user():
    month = new_user['month']
    consumption = new_user['consumption']

        # new_user['month'] = month
        # new_user['consumption'] = consumption
    try:
        name = new_user['name']
        email = new_user['email']
        password = new_user['password']
        phone = new_user['phone']
        county = new_user['county']
        city = new_user['city']
        roof_length = new_user['roof_length']
        roof_width = new_user['roof_width']

        old_info['month'] = month
        old_info['consumption'] = consumption


        user = User(name= name, email= email, password=generate_password_hash(password, method='sha256'), phone=phone, county=county, city=city, roof_length=roof_length, roof_width=roof_width, month=month, consumption=consumption, budget = 0)
        db.session.add(user)
        newStat = Stats.query.first()
        newStat.nr_users += 1
        db.session.commit()

        login_user(user, remember=True)
    except:
        pass

@auth.route("/consumatori", methods=["GET", "POST"])
def consumatori():

    # devices = {
    #     # W consumption per hour
    #     "air conditioning" : 600,
    #     'clothes dryer' : 3000,
    #     'iron' : 2400,
    #     'dishwasher' : 1600,
    #     'electric kettle' : 2000,
    #     'microwave oven' : 800 ,
    #     'desktop' : 350,
    #     'laptop' : 100,
    #     'refrigerator' : 200, 
    #     'television' : 110,
    #     'vacuum cleaner' : 1600,
    #     'washing machine' : 2000,
    #     'water heater' : 4000,
    #     'phone' : 8,
    #     'lights' : 14,
    #     'electric stove' : 1600,
    #     'power plant' : 2000,
    #     'electric car' : 100_000,
    #     'security camera' : 10,
    # }
    with open("AMIGOS/website/database/devices.json") as file:
        devices = json.load(file)

    data = request.form

    result = []
    my_consumption = 0
    if request.method == "POST":
        append_device_to_phone(["consumatori"])

        try:
            contor = int(data.get("contor"))
        except:
            contor = 1

        result = []
        for i in range(1, contor + 1):
            try:
                device = data.get(f"device {i}")
                try:
                    number = int(data.get(f"device {i} number"))
                except:
                    number = 1
                try:
                    units = int(data.get(f"device {i} units"))
                except:
                    units = 1
                measure = data.get(f"device {i} measure")
                result.append([device, number, units, measure])

                if device == "air conditioning" or device == "power plant":
                    try:
                        adaug = int(devices[device] * number * units * 30 / 1000)
                    except:
                        adaug = 1
                    append_device_to_phone([device, str(adaug)])
                else:

                    if measure == "non_stop":
                        my_consumption += devices[device] * number * 24 * 30
                    
                    if measure == "days":
                        my_consumption += devices[device] * number * units

                    if measure == "hours":
                        my_consumption += devices[device] * number *units * 30
                    
                    if measure == "minutes":
                        hours = units / 60
                        my_consumption += devices[device] * number * hours * 30
            except:
                pass

            

        my_consumption /= 1000 #to get in kW

        new_user['month'] = "April"
        new_user['consumption'] = my_consumption
        
        sign_in_new_user()
        return redirect(url_for('views.home', user=current_user))


    return render_template("consumatori.html", result = result, my_consumption = my_consumption, proba=new_user['phone'], user=current_user)
