import scrapy
import json


class PostsSpider(scrapy.Spider):
    name = "posts"

    our_urls = [ 0, 0 ]
    start_link = "https://www.solar-depot.ro/"
    
    lista_panouri = []
    lista_baterii = []
    lista_invertoare = []
    liste_obiecte = [ lista_panouri, lista_baterii, lista_invertoare ]

    def dump_lista_obiecte(self):
        with open('lista_obiecte.json', 'w') as f:
            json.dump(self.liste_obiecte, f, indent=4)


    def start_requests(self):
        yield scrapy.Request('https://www.solar-depot.ro/Panouri-Fotovoltaice', self.parse)
        yield scrapy.Request('https://www.solar-depot.ro/Baterii-Solare', self.parse)
        yield scrapy.Request('https://www.solar-depot.ro/Invertoare-Off_Grid-cu-regulator-de-incarcare', self.parse)
        yield scrapy.Request('https://www.solar-depot.ro/Invertoare-hibride', self.parse)

    # def parse_one(self, response):
    #     print("HERE_________________adsasdyatsd_____HERE", response.url)

    

    def parse(self, response):
        if response.url == "https://www.solar-depot.ro/Panouri-Fotovoltaice":
            urls = response.css('a::attr(href)').re(".+Panou-fotovoltaic.+")

        if response.url == "https://www.solar-depot.ro/Baterii-Solare":
            urls = response.css('a::attr(href)').re(".+Baterie-solara.+")  
        
        if response.url == "https://www.solar-depot.ro/Invertoare-Off_Grid-cu-regulator-de-incarcare" or response.url == "https://www.solar-depot.ro/Invertoare-hibride":
            urls = response.css('a::attr(href)').re(".+Invertor-hibrid.+")  
        
        for url in urls:
            if "culoare" in url or "HIBRID" in url:
                continue

            url = self.start_link + url
            if response.url == "https://www.solar-depot.ro/Panouri-Fotovoltaice":
                yield scrapy.Request(url, self.parse_panouri)

            if response.url == "https://www.solar-depot.ro/Baterii-Solare":
                yield scrapy.Request(url, self.parse_baterii)

            if response.url == "https://www.solar-depot.ro/Invertoare-Off_Grid-cu-regulator-de-incarcare" or response.url == "https://www.solar-depot.ro/Invertoare-hibride":
                yield scrapy.Request(url, self.parse_invertor)




    
    def parse_invertor(self, response):
        print("HERE______________________HERE", response.url)
        try: 
            keys = response.css('.caseta360 .specificatii .rand label::text').getall()
            values = response.css('.caseta360 .specificatii .rand::text').getall()

            for i in range(0, len(values)):
                values[i] = values[i].replace(' ', '').replace('\n', '').replace('\xa0', '')
        
            i = 0
            while i < len(values):
                if values[i] == '':
                    values.pop(i)
                    i -= 1
                    if i == -1:
                        i = 0
                i += 1

            print(values)


            poza = response.css('.caseta360 .poza a img::attr(src)').get()
            poza = self.start_link + poza


            invertor_name = response.url.split('/')[-1].replace('-', ' ')
            print("NAME____", invertor_name)


            price = response.css('.pret div::text')[1].get()
            
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            print('PRICE_______', price)

            my_dict = {
                "name" : invertor_name,
                "power" : int(values[-3]),
                "voltage" : int(values[-1]),
                "charging" : int(values[0]), 
                "price" : price,
                "picture" : poza,
                "link" : response.url,
            }

            self.lista_invertoare.append(my_dict)
#    "1" : {
#       "name": "Invertor MPPSOLAR 1012HSE tip hibrid",
#       "power": 1000,
#       "voltage": 40,
#       "charging": 50,
#       "price": 2639,
#       "picture": "../static/assets/pictures/Invertor_1.jpg",
#       "link": "https://www.solar-depot.ro/159/Invertor-hibrid-unda-pura-1000W-cu-regulator-PWM-50A-MPPSOLAR"
#   },

            print("MY_DICT_-----------------\n")
            print(my_dict)

        except:
            price = response.css('.pret div::text')[1].get()
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            my_dict = {
                "price" : price,
                "link" : response.url,
            }

            self.lista_invertoare.append(my_dict)

        with open('lista_obiecte.json', 'w') as f:
            json.dump(self.liste_obiecte, f, indent=4)

    def parse_baterii(self, response):
        print("HERE______________________HERE", response.url)
        try: 
            keys = response.css('.caseta360 .specificatii .rand label::text').getall()
            values = response.css('.caseta360 .specificatii .rand::text').getall()

            for i in range(0, len(values)):
                values[i] = values[i].replace(' ', '').replace('\n', '').replace('\xa0', '')
        
            i = 0
            while i < len(values):
                if values[i] == '':
                    values.pop(i)
                    i -= 1
                    if i == -1:
                        i = 0
                i += 1

            print(values)


            poza = response.css('.caseta360 .poza a img::attr(src)').get()
            poza = self.start_link + poza


            battery_name = response.url.split('/')[-1].replace('-', ' ')
            print("NAME____", battery_name)


            price = response.css('.pret div::text')[1].get()
            
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            print('PRICE_______', price)

            my_dict = {
                "name" : battery_name,
                "brand" : "Caranda",
                "type" : values[-2],
                "capacity" : int(values[0]), 
                "voltage" : int(values[-1]),
                "price" : price,
                "picture" : poza,
                "link" : response.url,
            }
            self.lista_baterii.append(my_dict)
    #          "1" : {
    #     "name" : "Baterie solara AGM GEL 55Ah",
    #     "brand": "Caranda",
    #     "type" : "AGM GEL ",
    #     "capacity" : 55,
    #     "voltage" : 12,
    #     "price" : 710,
    #     "picture" : "../static/assets/pictures/acumulator_1.jpg",
    #     "link" : "https://www.solar-depot.ro/108/Baterie-solara-AGM-GEL-55Ah"
    # },

            print("MY_DICT_-----------------\n")
            print(my_dict)

        except:
            price = response.css('.pret div::text')[1].get()
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            my_dict = {
                "price" : price,
                "link" : response.url,
            }

            self.lista_baterii.append(my_dict)

        with open('lista_obiecte.json', 'w') as f:
            json.dump(self.liste_obiecte, f, indent=4)
        

    def parse_panouri(self, response):
        # new urls
        print("HERE______________________HERE", response.url)
        try: 
            keys = response.css('.caseta360 .specificatii .rand label::text').getall()
            values = response.css('.caseta360 .specificatii .rand::text').getall()

            for i in range(0, len(values)):
                values[i] = values[i].replace(' ', '').replace('\n', '').replace('\xa0', '')
        
            i = 0
            while i < len(values):
                if values[i] == '':
                    values.pop(i)
                    i -= 1
                    if i == -1:
                        i = 0
                i += 1

            print(values)


            poza = response.css('.caseta360 .poza a img::attr(src)').get()
            poza = self.start_link + poza


            panel_name = response.url.split('/')[-1].replace('-', ' ')
            print("NAME____", panel_name)


            price = response.css('.pret div::text')[1].get()
            
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            print('PRICE_______', price)

            dimensiuni = values[2].split('x')
            dimensiuni[0] = int(dimensiuni[0]) / 1000
            dimensiuni[1] = int(dimensiuni[1]) / 1000

            my_dict = {
                "name" : panel_name,
                "type" : values[-1],
                "power" : int(values[-4]),
                "voltage" : float(values[-2]),
                "length" : max(dimensiuni[0], dimensiuni[1]),
                "width" : min(dimensiuni[0], dimensiuni[1]),
                "price" : price,
                "picture" : poza,
                "link" : response.url,
            }

            self.lista_panouri.append(my_dict)

            print("MY_DICT_-----------------\n")
            print(my_dict)

        except:
            price = response.css('.pret div::text')[1].get()
            price = price.replace(' lei', '').replace('.', '')
            price = int(price)

            my_dict = {
                "price" : price,
                "link" : response.url,
            }

            self.lista_panouri.append(my_dict)

        with open('lista_obiecte.json', 'w') as f:
            json.dump(self.liste_obiecte, f, indent=4)

#         "1": {
#     "name": "Panou fotovoltaic 55w",
#     "type": "policristalin",
#     "power": 55,
#     "voltage": 18.14,
#     "degrade": 0.8,
#     "length": 0.668,
#     "width": 0.620,
#     "price": 328,
#     "picture":"../static/assets/pictures/panou_55w.jpg",
#     "link":"https://www.solar-depot.ro/134/Panou-fotovoltaic-55w"
#   },


        # print(response.css('.caseta360 .specificatii .rand::text').getall())


    # def parse(self, response):
    #     page = response.url.split('/')[-1]

    #     print("HERE______________________HERE", response.url)

    #     filename = 'posts-%s.html' % page
    #     with open(filename, 'w') as f:
            
    #         try:
    #             i = -1
    #             while 1==1:
    #                 i += 1

    #                 if response.url == "https://www.solar-depot.ro/Panouri-Fotovoltaice":
    #                     # print("HERE______________________HERE", response.url)
    #                     end_link = response.css('.produs266 .poza a::attr(href)')[i].get()

    #                     link = self.start_link + end_link
    #                     if link not in self.our_urls:
    #                         self.our_urls.append(self.start_link + end_link)

    #                     f.write(response.css('.produs266 .poza a::attr(href)')[i].get())
    #                     f.write("\n")
    #                 else:
    #                     break
    #         except:
    #             pass

    #         f.write(f"{self.our_urls}")



